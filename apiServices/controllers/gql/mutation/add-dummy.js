import CitizenAppDummy from '../../../models/Citizen-App-Dummy.js'
import moment from 'moment-timezone';
import fetch from "node-fetch"

const controllerAddDummy = async (root, args, { user }) => {

  console.log("args", args)

  try {

    // * Obtener campos
    const { nameDummy, emailDummy } = args

    // * Agregar dateCL a los argumentos
    args.dateCL = moment().tz('America/Santiago').format()

    // * Insertar usuario
    const newDummy = new CitizenAppDummy({ ...args })

    console.log("newDummy", newDummy)

    const savedDummy = await newDummy.save()

    if (!savedDummy) {
      throw new Error('save-user-error')
    }

    // * Envío de email
    /*     const responseSendMail = await fetch(`${process.env.EXPRESS_URL}/api/send-email/citizen-app-welcome`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            mail: emailDummy,
            name: nameDummy
          }),
        });
    
        await responseSendMail.json(); */

    return savedDummy

  } catch (error) {
    throw new Error(error)
  }
}

export default controllerAddDummy
