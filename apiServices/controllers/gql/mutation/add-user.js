import CitizenAppUser from '../../../models/Citizen-App-User.js'
import { validateAddUser } from '../../../validations/all-validations.js'
import fetch from "node-fetch"
import moment from 'moment-timezone'

const controllerAddUser = async (root, args, { user }) => {
  // * Validar campos
  const { error } = validateAddUser.validate(args, { abortEarly: false })
  if (error) {
    throw new Error(error.details[0].message)
  }

  try {
    // * Obtener campos
    const { nameUser, emailUser } = args

    // * Insertar usuario
    const newUser = new CitizenAppUser({ ...args })
    const savedUser = await newUser.save()

    if (!savedUser) {
      throw new Error('save-user-error')
    }

    // * Envío de email
    const responseSendMail = await fetch(`${process.env.EXPRESS_URL}/api/send-email/citizen-app-welcome`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        mail: emailUser,
        name: nameUser
      }),
    });

    await responseSendMail.json();

    return savedUser

  } catch (error) {
    throw new Error(error)
  }
}

export default controllerAddUser