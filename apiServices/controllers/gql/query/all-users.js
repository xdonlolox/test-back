import CitizenAppUser from '../../../models/Citizen-App-User.js'

const controllerAllUsers = async (root, args, { user }) => {
  const users = await CitizenAppUser.find({})
  return users
}

export default controllerAllUsers