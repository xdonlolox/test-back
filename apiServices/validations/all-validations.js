import Joi from '@hapi/joi'

const validateAddUser = Joi.object({
  nameUser: Joi.string().min(6).max(255).regex(/^[a-zA-ZáéíóúñÑÁÉÍÓÚ ]+$/).required(),
  emailUser: Joi.string().min(6).max(255).required().email(),
})

export {
  validateAddUser
}