import { gql } from 'apollo-server'

const typeDefs = gql`
  type ReturnUser {
    _id: ID
    nameUser: String
    emailUser: String
  }

  type ReturnRegion {
    nameRegion: String
    numberRegion: String
  }

  type ReturnDummy {
    nameDummy: String
    emailDummy: String
    region: ReturnRegion
    date: String
    dateCL: String
  }

  type Query {
    allUsers: [ReturnUser]
    dummy: String
  }

  input InputRegion {
    nameRegion: String
    numberRegion: String
  }

  type Mutation {
    addUser (
      nameUser: String
      emailUser: String
    ): ReturnUser,
    addDummy (
      nameDummy: String
      emailDummy: String
      region: InputRegion
    ): ReturnDummy
  }
`;

export default typeDefs