// * DB Connect
import { } from '../db.js'

// * Queries controllers
import controllerAllUsers from './controllers/gql/query/all-users.js';
import controllerDummy from './controllers/gql/query/dummy.js';

// * Mutations controllers
import controllerAddUser from './controllers/gql/mutation/add-user.js';
import controllerAddDummy from './controllers/gql/mutation/add-dummy.js';

const resolvers = {
  Query: {
    allUsers: controllerAllUsers,
    dummy: controllerDummy
  },

  Mutation: {
    addUser: controllerAddUser,
    addDummy: controllerAddDummy
  }
};

export default resolvers