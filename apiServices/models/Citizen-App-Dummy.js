import mongoose from 'mongoose'

const regionSchema = mongoose.Schema({
  nameRegion: {
    type: String,
    required: true,
    max: 255
  },
  numberRegion: {
    type: String,
    required: true,
    max: 255
  },
})

const dummySchema = mongoose.Schema({
  nameDummy: {
    type: String,
    required: true,
    max: 255
  },
  emailDummy: {
    type: String,
    required: true,
    max: 255
  },
  region: regionSchema,
  date: {
    type: Date,
    default: Date.now
  },
  dateCL: {
    type: String
  }
})

export default mongoose.model('Citizen-Dummy', dummySchema)