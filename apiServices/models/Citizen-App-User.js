import mongoose from 'mongoose'

const userSchema = mongoose.Schema({
  nameUser: {
    type: String,
    required: true,
    max: 255
  },
  emailUser: {
    type: String,
    required: true,
    max: 255
  },
  date: {
    type: Date,
    default: Date.now
  },
  dateCL: {
    type: String
  }
})

export default mongoose.model('Citizen-App-User', userSchema)