import mongoose from 'mongoose'
import dotenv from 'dotenv'
dotenv.config()

const uri = process.env.DB_URI

if (uri == "") {
  console.log("No hay URI de conexión a la BD")
}

//const options = { useNewUrlParser: true, useUnifiedTopology: true };
const options = {};

const dbConnect = mongoose.connect(uri, options)
  .then(() => console.log('Base de datos conectada'))
  .catch(e => console.log('error db:', e))

export default dbConnect