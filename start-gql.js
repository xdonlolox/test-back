import { ApolloServer } from 'apollo-server'

// * TYPEDEFS
import typeDefs from './apiServices/type-defs.js';

// * RESOLVERS
import resolvers from './apiServices/resolvers.js';

// * Crear servidor Apollo
const server = new ApolloServer({
  typeDefs,
  resolvers,
});

// * Inicializar servidor
server.listen({ port: process.env.PORT_APOLLO || 4006 }).then(({ url }) => {
  console.log(`Servidor listo en ${url}`);
});
